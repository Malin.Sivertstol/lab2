package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

// TODO: Finish Fridge

public class Fridge implements IFridge{
    public ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();
    int max_size = 20;


    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() >= max_size) {
            return false;
        }
        return items.add(item);
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(!items.contains(item)){
            throw new NoSuchElementException("Item not in fridge!");
        }
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        for( FridgeItem item : items) {
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for (FridgeItem expiredItem : expiredFood) {
            items.remove(expiredItem);
        }
        return expiredFood;
    }




}
